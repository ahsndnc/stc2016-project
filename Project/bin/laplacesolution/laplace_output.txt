Laplace Method Circuit Solver Results:

*Current direction is clockwise through each loop.
*Element voltage drop polarity is with respect to the lower-numbered loop current through each element.

Operation Frequency: 5000.0

Resistance Matrix:
 5.00e+02  0.00e+00  2.00e+02  0.00e+00 
 0.00e+00  4.00e+02  0.00e+00  3.00e+02 
 2.00e+02  0.00e+00  3.00e+02  0.00e+00 
 0.00e+00  3.00e+02  0.00e+00  0.00e+00 

Capacitance Matrix:
 1.00e-05  1.00e-05  0.00e+00  0.00e+00 
 1.00e-05  0.00e+00  0.00e+00  0.00e+00 
 0.00e+00  0.00e+00  0.00e+00  1.00e-06 
 0.00e+00  0.00e+00  1.00e-06  5.00e-08 

Inductance Matrix:
 0.00e+00  0.00e+00  0.00e+00  0.00e+00 
 0.00e+00  1.00e-06  0.00e+00  0.00e+00 
 0.00e+00  0.00e+00  2.00e-06  0.00e+00 
 0.00e+00  0.00e+00  0.00e+00  5.00e-06 

Impedance Matrix:
{ 7.00e+02,-6.37e+00j} {-4.14e-05, 4.55e-03j} {-2.86e-01,-2.60e-03j} { 0.00e+00,-0.00e+00j} 
{-0.00e+00, 3.18e+00j} { 7.00e+02,-3.15e+00j} {-1.77e-05, 1.30e-03j} {-4.29e-01,-1.93e-03j} 
{-2.00e+02,-0.00e+00j} {-8.27e-03, 9.09e-01j} { 4.43e+02,-3.23e+01j} {-5.29e-03, 7.24e-02j} 
{-0.00e+00,-0.00e+00j} {-3.00e+02,-0.00e+00j} {-5.30e-03, 3.22e+01j} { 1.74e+02,-6.69e+02j} 

Voltage Source Vector:
 1.00e+01 * cos( 3.14e+04 * t + ( 0.00e+00)) Volts
 0.00e+00 * cos( 3.14e+04 * t + ( 0.00e+00)) Volts
 0.00e+00 * cos( 3.14e+04 * t + ( 0.00e+00)) Volts
 0.00e+00 * cos( 3.14e+04 * t + ( 0.00e+00)) Volts

Loop Current Vector:
 1.61e-02 * cos( 3.14e+04 * t + ( 1.03e+00)) Amps
 1.71e-04 * cos( 3.14e+04 * t + (-3.48e+01)) Amps
 6.43e-03 * cos( 3.14e+04 * t + ( 4.48e+00)) Amps
 3.28e-04 * cos( 3.14e+04 * t + (-1.02e+01)) Amps

Resistor Voltages:
Resistor R 1:  8.06e+00 * cos( 3.14e+04 * t + ( 1.03e+00)) Volts
Resistor R 1 3:  1.94e+00 * cos( 3.14e+04 * t + (-1.26e+00)) Volts
Resistor R 2:  6.83e-02 * cos( 3.14e+04 * t + (-3.48e+01)) Volts
Resistor R 2 4:  5.61e-02 * cos( 3.14e+04 * t + ( 1.22e+01)) Volts
Resistor R 3:  1.93e+00 * cos( 3.14e+04 * t + ( 4.48e+00)) Volts

Capacitor Voltages:
Capacitor C 1:  5.13e-02 * cos( 3.14e+04 * t + (-8.90e+01)) Volts
Capacitor C 1 2:  5.09e-02 * cos( 3.14e+04 * t + (-8.86e+01)) Volts
Capacitor C 3 4:  1.95e-01 * cos( 3.14e+04 * t + (-8.47e+01)) Volts
Capacitor C 4:  2.09e-01 * cos( 3.14e+04 * t + ( 7.98e+01)) Volts

Inductor Voltages:
Inductor L 2:  5.37e-06 * cos( 3.14e+04 * t + ( 5.52e+01)) Volts
Inductor L 3:  4.04e-04 * cos( 3.14e+04 * t + (-8.55e+01)) Volts
Inductor L 4:  5.16e-05 * cos( 3.14e+04 * t + ( 7.98e+01)) Volts

Resistor Currents:
Resistor R 1:  1.61e-02 * cos( 3.14e+04 * t + ( 1.03e+00)) Volts
Resistor R 1 3:  9.71e-03 * cos( 3.14e+04 * t + (-1.26e+00)) Volts
Resistor R 2:  1.71e-04 * cos( 3.14e+04 * t + (-3.48e+01)) Volts
Resistor R 2 4:  1.87e-04 * cos( 3.14e+04 * t + ( 1.22e+01)) Volts
Resistor R 3:  6.43e-03 * cos( 3.14e+04 * t + ( 4.48e+00)) Volts

Capacitor Currents:
Capacitor C 1:  1.61e-02 * cos( 3.14e+04 * t + ( 1.03e+00)) Volts
Capacitor C 1 2:  1.60e-02 * cos( 3.14e+04 * t + ( 1.39e+00)) Volts
Capacitor C 3 4:  6.11e-03 * cos( 3.14e+04 * t + ( 5.26e+00)) Volts
Capacitor C 4:  3.28e-04 * cos( 3.14e+04 * t + (-1.02e+01)) Volts

Inductor Currents:
Inductor L 2:  1.71e-04 * cos( 3.14e+04 * t + (-3.48e+01)) Volts
Inductor L 3:  6.43e-03 * cos( 3.14e+04 * t + ( 4.48e+00)) Volts
Inductor L 4:  3.28e-04 * cos( 3.14e+04 * t + (-1.02e+01)) Volts

Resistor Powers:
Average Power on Resistor R 1:    6.49e-02 Watts
Average Power on Resistor R 1 3:  9.42e-03 Watts
Average Power on Resistor R 2:    5.83e-06 Watts
Average Power on Resistor R 2 4:  5.25e-06 Watts
Average Power on Resistor R 3:    6.20e-03 Watts
