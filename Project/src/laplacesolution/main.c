#include "project.h"

int main()
{
   // Initialize variables and arrays

   int one = 1;
   int i,j,number_loops;

   double const pi = 3.14159265359;
   double frequency,omega;

   char input_file[] = "./laplace_input.txt";							// Specify filepath for input file
   char output_file[] = "./laplace_output.txt";							// Specify filepath for output file 

   number_loops = read_number(input_file); 							// Read nuber of loops from input file

   double *Resistance = mkl_malloc(number_loops*number_loops*sizeof(double),64);		// Double arrays to hold R,L,C values
   double *Capacitance = mkl_malloc(number_loops*number_loops*sizeof(double),64); 
   double *Inductance = mkl_malloc(number_loops*number_loops*sizeof(double),64);
   double *P_R = mkl_malloc(number_loops*number_loops*sizeof(double),64);

   MKL_Complex16 Impedance[number_loops*number_loops];						// Complex arrays to hold Z,V,I
   MKL_Complex16 Current[number_loops];
   MKL_Complex16 Voltage[number_loops];
   MKL_Complex16 Vtemp[number_loops];

   MKL_Complex16 V_R[number_loops*number_loops];						// Complex arrays to hold voltages across each element
   MKL_Complex16 V_C[number_loops*number_loops];
   MKL_Complex16 V_L[number_loops*number_loops];

   MKL_Complex16 Z_R[number_loops*number_loops];						// Complex arrays to hold impedances of each element
   MKL_Complex16 Z_C[number_loops*number_loops];
   MKL_Complex16 Z_L[number_loops*number_loops];

   //MKL_Complex16 P_R[number_loops*number_loops];

   MKL_INT n = number_loops;									// Complex variables as inputs to zgesv funct
   MKL_INT nrhs = 1;										// Number of right hand side vectors
   MKL_INT lda = number_loops;									// Leading dimension of impedance matrix
   MKL_INT ldb = nrhs;										// Leading dimension of voltage matrix
   MKL_INT info;			
   MKL_INT ipiv[number_loops];

   MKL_Complex16 ResistorCurrent[number_loops*number_loops];
   MKL_Complex16 CapacitorCurrent[number_loops*number_loops];
   MKL_Complex16 InductorCurrent[number_loops*number_loops];

   frequency = read_input(input_file,Resistance,Capacitance,Inductance,Voltage,number_loops);	// Fill R,C,L value matrices; returns frequency value
   omega = 2 * pi * frequency;							

   for(i=0;i<number_loops;i++)									// Duplicate Voltage into Vtemp
      Vtemp[i] = Voltage[i];

   calc_impedance(Resistance,Capacitance,Inductance,Impedance,Z_R,Z_C,Z_L,number_loops,omega);	// Fill impedance matrices using R,C,L matrices

   LAPACKE_zgesv(LAPACK_ROW_MAJOR,n,nrhs,Impedance,lda,ipiv,Vtemp,ldb);				// Solve ZI = R for I. Answer stored in Vtemp

   for(i=0;i<number_loops;i++)									// Move that result into I
      Current[i] = Vtemp[i];

   calc_voltages(Z_R,Z_C,Z_L,V_R,V_C,V_L,Current,number_loops);					// Calculate and fill V matrices with voltage across each element

   calc_power(Resistance, V_R, P_R, number_loops);
   calc_currents(Resistance, Current, ResistorCurrent, number_loops);
   calc_currents(Capacitance, Current, CapacitorCurrent, number_loops);
   calc_currents(Inductance, Current, InductorCurrent, number_loops); 
   write_output(output_file,Impedance,Voltage,Current,Resistance,Capacitance,Inductance,V_R,V_C,V_L,P_R,ResistorCurrent,CapacitorCurrent,InductorCurrent,number_loops,frequency, omega);	// Write desired values to output file
   return 0;
}

