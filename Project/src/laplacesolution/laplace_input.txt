*Laplace Circuit Solver Input File
*Formatting is specific. Please follow instructions for each category.

*LOOP INFORMATION. Place one blank space followed by value after each variable name.
number_loops 4
frequency 5000.

* V: voltage source, R: resistance, C: capacitance, L: inductance
* List integers representing the two loops that each element is in contact with, followed by its value. If only one loop, then list the same loop number twice.
* For voltage polarity, have the voltage rise by in the direction of the first loop current number listed.

*VOLTAGES:
V 1 1 10

*RESISTORS
R 1 1 500
R 1 3 200
R 3 3 300
R 2 2 400
R 2 4 300

*CAPACITORS
C 1 1 10e-6
C 1 2 10e-6
C 4 4 50e-9
C 3 4 1e-6

*INDUCTORS
L 3 3 2e-6
L 2 2 1e-6
L 4 4 5e-6

