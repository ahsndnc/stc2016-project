// Header file for Project
// Laplace Transform Implementation

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "mkl.h"

#define Complex MKL_Complex16

#define Resistance(i,j) Resistance[(i) + number_loops*(j)]
#define Capacitance(i,j) Capacitance[(i) + number_loops*(j)]
#define Inductance(i,j) Inductance[(i) + number_loops*(j)]
#define Impedance(i,j) Impedance[(i) + number_loops*(j)]
#define V(i,j) V[(i) * number_loops*(j)]
#define V_R(i,j) V_R[(i) + number_loops*(j)]
#define V_C(i,j) V_C[(i) + number_loops*(j)]
#define V_L(i,j) V_L[(i) + number_loops*(j)]
#define Z_R(i,j) Z_R[(i) + number_loops*(j)]
#define Z_C(i,j) Z_C[(i) + number_loops*(j)]
#define Z_L(i,j) Z_L[(i) + number_loops*(j)]
#define Vmatrix(i,j) Vmatrix[(i) + number_loops*(j)]
#define Itemp(i,j) Itemp[(i) + number_loops*(j)]
#define done(i,j) done[(i) + number_loops*(j)]
#define zero(i,j) zero[(i) + number_loops*(j)]
#define P_R(i,j) P_R[(i) + number_loops*(j)]
#define ElementalCurrent(i,j) ElementalCurrent[(i) + number_loops*(j)]
#define ResistorCurrent(i,j) ResistorCurrent[(i) + number_loops*(j)]
#define CapacitorCurrent(i,j) CapacitorCurrent[(i) + number_loops*(j)]
#define InductorCurrent(i,j) InductorCurrent[(i) + number_loops*(j)]
double read_input(char *filename,double *Resistance,double *Capacitance,double *Inductance,Complex *Voltage,int number_loops);

void calc_impedance(double *Resistance,double *Capacitance,double *Inductance,Complex *Impedance,Complex *Z_R,Complex *Z_C,Complex *Z_L,int number_loops,double omega);
void calc_voltages(Complex *Z_R,Complex *Z_C,Complex *Z_L,Complex *V_R,Complex *V_C,Complex *V_L,Complex *Current,int number_loops);
void write_output(char *filename,Complex *Impedance,Complex *Voltage,Complex *Current,double *Resistance,double *Capacitance,double *Inductance,Complex *V_R,Complex *V_C,Complex *V_L, double *P_R, Complex *ResistorCurrent, Complex *CapacitorCurrent, Complex *InductorCurrent,int number_loops,double frequency, double omega);
void calc_power(double *Resistance, Complex *V_R, double *P_R, int number_loops);void calc_currents(double *ImMatrix, Complex *Current, Complex *ElementalCurrent, int number_loops);

Complex c_subtract(Complex C1,Complex C2);
Complex c_multiply(Complex C1,Complex C2);
double calc_abs(double a, double b);
double calc_phase(double a, double b);

