#include "project.h"

int read_number(char *filename)				// Retrieves number of loops from file and returns as integer
{
   int number_loops = 0, found = 0;
   char letter,skip;
   char dump[100];
   
   FILE *fp;
   fp = fopen(filename, "r");
   
   while(found == 0)					// While number of loops has not been found..
   {
      letter = fgetc(fp);				// Read first character of line      

      switch(letter)					// Case statements for value of letter
      {
         case '\n':					// If first char of line is newline
            break;					// Then do nothing
         case 'n': case 'N':				// If first char of line is n or N
            found = 1;					// Then specify that number of loops has been found
	    fscanf(fp, "%s", dump);			// And read the rest of the string and dump it
            skip = fgetc(fp);				// Read white space character
            fscanf(fp, "%d", &number_loops);		// And read the int value of number of loops
            break;
	 default:					// If none of above, then dump rest of line to move to next
            fgets(dump, 100, fp);
	    break;
      }
   }					 

   fclose(fp);
   return number_loops;					// Return number of loops to calling program
}

double read_input(char *filename,double *Resistance,double *Capacitance,double *Inductance,Complex *Voltage,int number_loops)
{
	// Parse input file to fill the arrays as appropriate and return frequency
   int i,j;
   char letter = 'a',skip;
   char dump[100];
   double frequency,R,L,C,V;
   double *Vmatrix = mkl_malloc(number_loops*number_loops*sizeof(double),64);

   for(i=0;i<number_loops;i++)				// Initialize R,L,C,V matrices to 0
   {
      Voltage[i].real = 0.0;
      Voltage[i].imag = 0.0;
      for(j=0;j<number_loops;j++)
      {
         Resistance(i,j) = 0.0;
         Capacitance(i,j) = 0.0;
         Inductance(i,j) = 0.0;
         Vmatrix(i,j) = 0.0;
      }
   }

   FILE *fp;
   fp = fopen(filename, "r");

   while(letter != -1)                                  // While EOF(-1) has not been found..
   {
      letter = fgetc(fp);                               // Read first character of line      

      switch(letter)                                    // Case statements for value of letter
      {
         case '\n':                                     // If first char of line is newline
            break;                                      // Then do nothing
         case 'f': case 'F':                            // If first char of line is n or N
            fscanf(fp, "%s", dump);                     // And read the rest of the string and white space then dump it       
            skip = fgetc(fp);				// Read white space character
            fscanf(fp, "%lf", &frequency);               // And read the double value of frequency       
            break;
         case 'r': case 'R':				// If char is r/R, then resistor has been found
	    skip = fgetc(fp);
            fscanf(fp, "%d", &i);			// First int found will be i value
            skip = fgetc(fp);
            fscanf(fp, "%d", &j);			// Second int found will be j value
            skip = fgetc(fp);
            fscanf(fp, "%lf", &R);			// Next double value will be Resistance
	    Resistance(i-1,j-1) = R;			// Place value in Resistance matrix as appropriate
            Resistance(j-1,i-1) = R;
            break;
         case 'c': case 'C':				// If char is c/C, then capacitor has been found
	    skip = fgetc(fp);
            fscanf(fp, "%d", &i);
            skip = fgetc(fp);
	    fscanf(fp, "%d", &j);
            skip = fgetc(fp);
            fscanf(fp, "%lf", &C);
	    Capacitance(i-1,j-1) = C;			// Place value in Capacitance matrix as appropriate
            Capacitance(j-1,i-1) = C;
            break;
         case 'l': case 'L':				// If char is l/L, then inductor has been found
            skip = fgetc(fp);
            fscanf(fp, "%d", &i);
            skip = fgetc(fp);
            fscanf(fp, "%d", &j);
            skip = fgetc(fp);
            fscanf(fp, "%lf", &L);
            Inductance(i-1,j-1) = L;			// Place value in Inductance matrix as appropriate
            Inductance(j-1,i-1) = L;
            break;
         case 'v': case 'V':				// If char is v/V, then voltage has been found
            skip = fgetc(fp);
            fscanf(fp, "%d", &i);
            skip = fgetc(fp);
            fscanf(fp, "%d", &j);
            skip = fgetc(fp);
            fscanf(fp, "%lf", &V);
            Vmatrix(i-1,j-1) = V;
         default:                                       // If none of above, then dump rest of line to move to next
            fgets(dump, 100, fp);
            break;
      }
   }                                     

   for(i=0;i<number_loops;i++)				// Fill Voltage array with Vmatrix values
   {
      for(j=0;j<number_loops;j++)
      {
         Voltage[i].real = Voltage[i].real + Vmatrix(i,j);
         if(i != j)
            Voltage[i].real = Voltage[i].real - Vmatrix(j,i);
      }
   }

   fclose(fp);
   return frequency;                                    // Return number of loops to calling program
}

void calc_impedance(double *Resistance,double *Capacitance,double *Inductance,Complex *Impedance,Complex *Z_R,Complex *Z_C,Complex *Z_L,int number_loops,double omega)
{
   // Transform R,L,C matrices into complex impedance matrices
   int i,j,k;
   double R,L,C;

   for(i=0;i<number_loops;i++)				// Initialize all elements to 0.0 + 0.0j
   {
      for(j=0;j<number_loops;j++)
      {
         Z_R(i,j).real = 0.0;
         Z_R(i,j).imag = 0.0;
         Z_C(i,j).real = 0.0;
         Z_C(i,j).imag = 0.0;
         Z_L(i,j).real = 0.0;
         Z_L(i,j).imag = 0.0;
         Impedance(i,j).real = 0.0;
         Impedance(i,j).imag = 0.0;
      }
   }
 
   for(i=0;i<number_loops;i++)				// Set element impedances with respect to R,C,L values
   {
      for(j=0;j<number_loops;j++)
      {
         Z_R(i,j).real = Resistance(i,j);
         if(Capacitance(i,j) != 0.0)
            Z_C(i,j).imag = -(1.0/(omega*Capacitance(i,j)));
         Z_L(i,j).imag = omega*Inductance(i,j);
      }
   }

   for(i=0;i<number_loops;i++)				// Set Impedance matrix elements as appropriate
   {
      for(j=0;j<number_loops;j++)
      {
         R = 0.0;
         L = 0.0;
         C = 0.0;
         if(i == j)					// For each diagonal element, R=sum(R(i,k) over k for each i. Similar for C,L
         {
            for(k=0;k<number_loops;k++)
            {
               R = R + Z_R(i,k).real;
               L = L + Z_L(i,k).imag;
               C = C + Z_C(i,k).imag;
            }
            Impedance(i,j).real = R;
            Impedance(i,j).imag = L + C;
         }
         else						// For all other elements, Z_R = -R. Similar for C,L
         {
            Impedance(i,j).real = -Z_R(i,j).real;
            Impedance(i,j).imag =  -(Z_C(i,j).imag + Z_L(i,j).imag);
         }           
      }
   }
}

void calc_voltages(Complex *Z_R,Complex *Z_C,Complex *Z_L,Complex *V_R,Complex *V_C,Complex *V_L,Complex *Current,int number_loops)
{							// Calculate the voltage drop across each element and place into V_R,V_C,V_L matrices
   int i,j;
   Complex C;

   for(i=0;i<number_loops;i++)				// Using V=IZ and complex numbers, compute voltage drop across each element
   {					  		// Note that polarity is with respect to the i-value loop current
      for(j=0;j<number_loops;j++)
      {
         if(i != j)
         {
            C = c_subtract(Current[i],Current[j]);	// C = Current[i] - Current[j];
            V_R(i,j) = c_multiply(Z_R(i,j),C);		// V_R(i,j) = Z_R(i,j) * C
            V_C(i,j) = c_multiply(Z_C(i,j),C);
            V_L(i,j) = c_multiply(Z_L(i,j),C);
         }
         else
         {
            C = Current[i];
            V_R(i,j) = c_multiply(Z_R(i,j),C);
            V_C(i,j) = c_multiply(Z_C(i,j),C);
            V_L(i,j) = c_multiply(Z_L(i,j),C);
         }
      }
   }
}

Complex c_subtract(Complex C1,Complex C2)
{		// Performs a subtraction of two complex numbers and returns the result, Result = C1 - C2
   Complex Result;
   Result.real = C1.real - C2.real;
   Result.imag = C1.imag - C2.imag;
   
   return Result;
}
   
Complex c_multiply(Complex C1,Complex C2)
{
   Complex Result;
   Result.real = (C1.real * C2.real) - (C1.imag * C2.imag);
   Result.imag = (C1.real * C2.imag) + (C1.imag * C2.real);

   return Result;
}

void write_output(char *filename,Complex *Impedance,Complex *Voltage,Complex *Current,double *Resistance,double *Capacitance,double *Inductance,Complex *V_R,Complex *V_C,Complex *V_L, double *P_R, Complex *ResistorCurrent, Complex *CapacitorCurrent, Complex *InductorCurrent, int number_loops,double frequency, double omega)
{
   // Write output values to output file
   int i,j,n=number_loops;
   int *done = mkl_malloc(number_loops*number_loops*sizeof(int),64);
   int *zero = mkl_malloc(number_loops*number_loops*sizeof(int),64);   

   FILE* fp;
   fp = fopen(filename, "w+");   

   fprintf(fp, "Laplace Method Circuit Solver Results:\n\n");
   fprintf(fp, "*Current direction is clockwise through each loop.\n");
   fprintf(fp, "*Element voltage drop polarity is with respect to the lower-numbered loop current through each element.\n");
   fprintf(fp, "\nOperation Frequency: %.1f\n", frequency);
   
   for(i=0;i<number_loops;i++)								// Initialize zero as matrix of 0's
   {
      for(j=0;j<number_loops;j++)
         zero(i,j) = 0;
   }

   fprintf(fp, "\nResistance Matrix:\n");						// Print Resistance Matrix
   for(i=0;i<number_loops;i++)
   {
      for(j=0;j<number_loops;j++)
         fprintf(fp, "%9.2e ", Resistance(i,j));
      fprintf(fp, "\n");
   }

   fprintf(fp, "\nCapacitance Matrix:\n");						// Print capacitance Matrix
   for(i=0;i<number_loops;i++)
   {
      for(j=0;j<number_loops;j++)
         fprintf(fp, "%9.2e ", Capacitance(i,j));
      fprintf(fp, "\n");
   }

   fprintf(fp, "\nInductance Matrix:\n");						// Print Inductance Matrix
   for(i=0;i<number_loops;i++)
   {
      for(j=0;j<number_loops;j++)
         fprintf(fp, "%9.2e ", Inductance(i,j));
      fprintf(fp, "\n");
   }

   fprintf(fp, "\nImpedance Matrix:\n");						// Print complex Impedance Matrix
   for(i=0;i<number_loops;i++)
   {
      for(j=0;j<number_loops;j++)
         fprintf(fp, "{%9.2e,%9.2ej} ", Impedance(i,j).real, Impedance(i,j).imag);
      fprintf(fp, "\n");
   }

   fprintf(fp, "\nVoltage Source Vector:\n");							// Print RHS complex Voltage Matrix
   for(i=0;i<number_loops;i++)
    fprintf(fp, "%9.2e * cos(%9.2e * t + (%9.2e)) Volts\n", calc_abs(Voltage[i].real, Voltage[i].imag),omega, calc_phase(Voltage[i].real, Voltage[i].imag));


   fprintf(fp, "\nLoop Current Vector:\n");							// Print complex Current Matrix
   for(i=0;i<number_loops;i++)
    fprintf(fp, "%9.2e * cos(%9.2e * t + (%9.2e)) Amps\n", calc_abs(Current[i].real, Current[i].imag),omega, calc_phase(Current[i].real, Current[i].imag));


   cblas_scopy(n*n,(float*)zero,1,(float*)done,1);					// Set done = 0 for all elements to be used in displaying element voltages
											// in order to avoid repeat element listings of opposite polarity
   fprintf(fp, "\nResistor Voltages:\n");
   for(i=0;i<number_loops;i++)
   {
      for(j=0;j<number_loops;j++)
      {
         if((done(j,i) == 0) && ((V_R(i,j).real != 0.0) || (V_R(i,j).imag != 0.0)))
         {
	    if(i != j)
            fprintf(fp, "Resistor R %d %d: %9.2e * cos(%9.2e * t + (%9.2e)) Volts\n", i+1, j+1, calc_abs(V_R(i,j).real, V_R(i,j).imag),omega, calc_phase(V_R(i,j).real, V_R(i,j).imag));

            else
               fprintf(fp, "Resistor R %d: %9.2e * cos(%9.2e * t + (%9.2e)) Volts\n", i+1, calc_abs(V_R(i,j).real, V_R(i,j).imag),omega, calc_phase(V_R(i,j).real, V_R(i,j).imag));

         }
         done(i,j) = 1;
      }
   }

   cblas_scopy(n*n,(float*)zero,1,(float*)done,1);

   fprintf(fp, "\nCapacitor Voltages:\n");
   for(i=0;i<number_loops;i++)
   {
      for(j=0;j<number_loops;j++)
      {
         if((done(j,i) == 0) && ((V_C(i,j).real != 0.0) || (V_C(i,j).imag != 0.0)))
         {
            if(i != j)
               fprintf(fp, "Capacitor C %d %d: %9.2e * cos(%9.2e * t + (%9.2e)) Volts\n", i+1, j+1, calc_abs(V_C(i,j).real, V_C(i,j).imag),omega, calc_phase(V_C(i,j).real, V_C(i,j).imag));

            else
               fprintf(fp, "Capacitor C %d: %9.2e * cos(%9.2e * t + (%9.2e)) Volts\n", i+1, calc_abs(V_C(i,j).real, V_C(i,j).imag),omega, calc_phase(V_C(i,j).real, V_C(i,j).imag));

         }
         done(i,j) = 1;
      }
   }
   
   cblas_scopy(n*n,(float*)zero,1,(float*)done,1);
   
   fprintf(fp, "\nInductor Voltages:\n");
   for(i=0;i<number_loops;i++)
   {
      for(j=0;j<number_loops;j++)
      {
         if((done(j,i) == 0) && ((V_L(i,j).real != 0.0) || (V_L(i,j).imag != 0.0)))
         {
            if(i != j)
               fprintf(fp, "Inductor L %d %d: %9.2e * cos(%9.2e * t + (%9.2e)) Volts\n", i+1, j+1, calc_abs(V_L(i,j).real, V_L(i,j).imag),omega, calc_phase(V_L(i,j).real, V_L(i,j).imag));

            else
               fprintf(fp, "Inductor L %d: %9.2e * cos(%9.2e * t + (%9.2e)) Volts\n", i+1, calc_abs(V_L(i,j).real, V_L(i,j).imag),omega, calc_phase(V_L(i,j).real, V_L(i,j).imag));
   }
         done(i,j) = 1;
      }
   }
    cblas_scopy(n*n,(float*)zero,1,(float*)done,1);
   
   fprintf(fp, "\nResistor Currents:\n");
   for(i=0;i<number_loops;i++)
   {
      for(j=0;j<number_loops;j++)
      {
         if((done(j,i) == 0) && ((ResistorCurrent(i,j).real != 0.0) || (ResistorCurrent(i,j).imag != 0.0)))
         {
            if(i != j)
               fprintf(fp, "Resistor R %d %d: %9.2e * cos(%9.2e * t + (%9.2e)) Volts\n", i+1, j+1, calc_abs(ResistorCurrent(i,j).real, ResistorCurrent(i,j).imag),omega, calc_phase(ResistorCurrent(i,j).real, ResistorCurrent(i,j).imag));

            else
               fprintf(fp, "Resistor R %d: %9.2e * cos(%9.2e * t + (%9.2e)) Volts\n", i+1, calc_abs(ResistorCurrent(i,j).real, ResistorCurrent(i,j).imag),omega, calc_phase(ResistorCurrent(i,j).real, ResistorCurrent(i,j).imag));
   }
         done(i,j) = 1;
      }
   }
    cblas_scopy(n*n,(float*)zero,1,(float*)done,1);
   
   fprintf(fp, "\nCapacitor Currents:\n");
   for(i=0;i<number_loops;i++)
   {
      for(j=0;j<number_loops;j++)
      {
         if((done(j,i) == 0) && ((CapacitorCurrent(i,j).real != 0.0) || (CapacitorCurrent(i,j).imag != 0.0)))
         {
            if(i != j)
               fprintf(fp, "Capacitor C %d %d: %9.2e * cos(%9.2e * t + (%9.2e)) Volts\n", i+1, j+1, calc_abs(CapacitorCurrent(i,j).real, CapacitorCurrent(i,j).imag),omega, calc_phase(CapacitorCurrent(i,j).real, CapacitorCurrent(i,j).imag));

            else
               fprintf(fp, "Capacitor C %d: %9.2e * cos(%9.2e * t + (%9.2e)) Volts\n", i+1, calc_abs(CapacitorCurrent(i,j).real, CapacitorCurrent(i,j).imag),omega, calc_phase(CapacitorCurrent(i,j).real, CapacitorCurrent(i,j).imag));
   }
         done(i,j) = 1;
      }
   }
    cblas_scopy(n*n,(float*)zero,1,(float*)done,1);
   
   fprintf(fp, "\nInductor Currents:\n");
   for(i=0;i<number_loops;i++)
   {
      for(j=0;j<number_loops;j++)
      {
         if((done(j,i) == 0) && ((InductorCurrent(i,j).real != 0.0) || (InductorCurrent(i,j).imag != 0.0)))
         {
            if(i != j)
               fprintf(fp, "Inductor L %d %d: %9.2e * cos(%9.2e * t + (%9.2e)) Volts\n", i+1, j+1, calc_abs(InductorCurrent(i,j).real, InductorCurrent(i,j).imag),omega, calc_phase(InductorCurrent(i,j).real, InductorCurrent(i,j).imag));

            else
               fprintf(fp, "Inductor L %d: %9.2e * cos(%9.2e * t + (%9.2e)) Volts\n", i+1, calc_abs(InductorCurrent(i,j).real, InductorCurrent(i,j).imag),omega, calc_phase(InductorCurrent(i,j).real, InductorCurrent(i,j).imag));
   }
         done(i,j) = 1;
      }
   }
   cblas_scopy(n*n,(float*)zero,1,(float*)done,1);
    fprintf(fp, "\nResistor Powers:\n");
   for(i=0;i<number_loops;i++)
   {
      for(j=0;j<number_loops;j++)
      {
         if((done(j,i) == 0) && (P_R(i,j)!= 0.0))
         {
            if(i != j)
               fprintf(fp, "Average Power on Resistor R %d %d: %9.2e Watts\n", i+1, j+1, P_R(i,j));
            else
               fprintf(fp, "Average Power on Resistor R %d:   %9.2e Watts\n", i+1, P_R(i,j));
         }
         done(i,j) = 1;
      }
   }
   fclose(fp);
}
void calc_power(double *Resistance, Complex *V_R, double *P_R, int number_loops){
   int i, j;

   for (i=0; i<number_loops; i++){
      for (j=i; j<number_loops; j++){
         if(Resistance(i,j) != 0){
            double abs = calc_abs(V_R(i,j).real,V_R(i,j).imag);
            P_R(i,j) = abs*abs/2/Resistance(i,j); //Calculating with rms value

         }
      }
   }

}

double calc_abs(double a, double b){
   return sqrt((a*a+b*b));
}

double calc_phase(double a, double b){
   double pi = 3.14159265359;
   double val = 180.0 / pi;
   if(b!=0)
      return (atan(b/a)*val);
   return 0;
}
void calc_currents(double *Resistance, Complex *Current, Complex *ElementalCurrent, int number_loops){
   int i,j;
   for(i=0; i < number_loops; i++){
      for(j=i; j< number_loops; j++){
        if(Resistance(i,j)!=0){ 
	if(i!=j){
            ElementalCurrent(i,j) = c_subtract(Current[i], Current[j]);
         }
         else{
            ElementalCurrent(i,j) = Current[i];
         }
	}
	else{
	    ElementalCurrent(i,j).real=0;
	    ElementalCurrent(i,j).imag=0;
	}
      }
   }

}
