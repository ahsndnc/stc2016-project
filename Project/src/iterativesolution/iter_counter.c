#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){

FILE *fp;
int n_loops, n_connect;
int space, endline;
int i, j, k, m, ii, jj, kk;
int *totals;
int *read_totals;
char letter, stringval[100];
double val;
int val_int;


fp = fopen("./iter_input.txt","rw");

totals = malloc(sizeof(int)*8);
read_totals = malloc(sizeof(int)*8);
n_connect = 0;
n_loops = 0;

for(i=0;i<3;i++){
    while((letter = fgetc(fp)) != '\n'){
    }
}

k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "total_simulation_time= %lf", &val); //parse for first entry, n_loops

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "time_step= %lf", &val);

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_recordings= %d", &i);

for(i=0;i<2;i++){
    while((letter = fgetc(fp)) != '\n'){
    }
}

k=0;
memset(stringval, '\0', strlen(stringval));
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_loops= %d", &read_totals[6]);

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_emfs_ac= %d", &read_totals[0]);

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_emfs_dc= %d", &read_totals[1]);

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_resistors= %d", &read_totals[2]);

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_capacitors= %d", &read_totals[3]);

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_inductors= %d", &read_totals[4]);

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_emfs_special= %d", &read_totals[5]);

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_connections= %d", &read_totals[7]);

space = 0;



// actually read number of values in AC EMFS
memset(stringval, '\0', sizeof(stringval));
while((letter = fgetc(fp)) != '\n'){
}


for(i=0;i<7;i++){
    totals[i] = 0;
    while((letter = fgetc(fp)) != '\n'){
    }
    while((letter=fgetc(fp)) != '\n'){
        totals[i] += 1;
        k = 0;
        stringval[k] = letter;
        endline = 0;
        k = 0;
        while(space == 0){
            letter = fgetc(fp);//section that reads character (letter)
            if(letter == ' '){
                space = 1;
                k=0;
                memset(stringval, '\0', strlen(stringval));
            }else if(letter != '\n'){
                stringval[k] = letter;
                k++;
            }else{
                space = 1;
                endline = 1;
                k=0;
                memset(stringval, '\0', strlen(stringval));                
            }
        }
        space = 0;
        if(i == 0){
            for(m=0;m<2;m++){
                while(space == 0){
                    letter = fgetc(fp);//section that reads character (letter)
                    if(letter == ' '){
                        space = 1;
                        k=0;
                        memset(stringval, '\0', strlen(stringval));
                    }else if(letter != '\n'){
                        stringval[k] = letter;
                        k++;
                    }else{
                        space = 1;
                        endline = 1;
                        k=0;
                        memset(stringval, '\0', strlen(stringval));
                    }
                }
                space = 0;
            }
        }else if(i == 3){
            while(space == 0){
                letter = fgetc(fp);//section that reads character (letter)
                if(letter == ' '){
                    space = 1;
                    k=0;
                    memset(stringval, '\0', strlen(stringval));
                }else if(letter != '\n'){
                    stringval[k] = letter;
                    k++;
                }else{
                    space = 1;
                    endline = 1;
                    k=0;
                    memset(stringval, '\0', strlen(stringval));
                }
            }
            space = 0;
        }

        space = 0;
        while(endline == 0){
            k = 0;
            while(space == 0){
                letter = fgetc(fp);//section that reads character(letter)
                if(letter == ' '){
                    space = 1;
                    sscanf(stringval, "%d", &val_int);
                    if(val_int > n_loops){
                        n_loops = val_int;
                    } else if (-val_int > n_loops){
                        n_loops = -val_int;
                    }
                    n_connect += 1;
                    k=0;
                    memset(stringval, '\0', strlen(stringval));
                } else if(letter != '\n'){
                    stringval[k] = letter;
                    k++;
                } else {
                    space = 1;
                    endline = 1;
                    sscanf(stringval, "%d", &val_int);
                    if(val_int > n_loops){
                        n_loops = val_int;
                    } else if (-val_int > n_loops){
                        n_loops = -val_int;
                    }
                    n_connect += 1;
                    k=0;
                    memset(stringval, '\0', strlen(stringval));
                }

            }
            space = 0;
        }
        endline = 0;
    }
} //end of for loop
fclose(fp);

j = 0;
totals[6] = n_loops;
totals[7] = n_connect;


i=6;
fprintf(stdout,"n_loops: read max loop number of %d, file says %d ", n_loops, read_totals[i]);
fprintf(stdout,"and has %d loop current initializations\n",totals[i]);
i=0;
fprintf(stdout,"n_emfs_ac: read %d, file says %d\n", totals[i], read_totals[i]);
i=1;
fprintf(stdout,"n_emfs_dc: read %d, file says %d\n", totals[i], read_totals[i]);
i=2;
fprintf(stdout,"n_resistors: read %d, file says %d\n", totals[i], read_totals[i]);
i=3;
fprintf(stdout,"n_capacitors: read %d, file says %d\n", totals[i], read_totals[i]);
i=4;
fprintf(stdout,"n_inductors: read %d, file says %d\n", totals[i], read_totals[i]);
i=5;
fprintf(stdout,"n_emfs_special: read %d, file says %d\n", totals[i], read_totals[i]);
i=7;
fprintf(stdout,"n_connections: read %d, file says %d\n", totals[i], read_totals[i]);
fflush(stdout);

} //end of main



