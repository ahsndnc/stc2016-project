
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include "mkl.h"
#include "special_emf.h"

#define twopi 6.28318530718

int main(){

FILE *fbin, *fcurrent, *felement, *fsingleAC;

// loop, node, and element totals
int n_loops, n_elements, n_emf_ac, n_emf_dc, n_res, n_cap, n_ind, n_emf_spec;

// element values
double *values, *freq_and_phase;
double *single_AC_voltages;

/* // element begin and end node, unsure if want to use
int *emf_1, *emf_2, *res_1, *res_2, *cap_1, *cap_2, *ind_1, *ind_2;
*/

// voltage and current values
double *loop_current, *loop_current_old, *loop_voltage, *capacitor_voltage;
double resistor_voltage, capacitor_voltage_temp, inductor_voltage;

// impedance matrix
double *imp_matrix;

// usage matrix, holds information on which elements are in which loops
// and in which "direction"
// made list-style, (n_elements) x (n_loops+1), first entry is m=#loops used,
// next m entries are the  loops used by given element, rest are 0
int *usage, *totals, *repeats;

int len1, len2, len3, len4, len5, len6;

int res_pos, cap_pos; // holds location of where resistor/capacitors start in usage array

// dummy variables
int i,j,k, pos;
int ii, jj, kk;
int loop1, loop2, loops_used;
double val, frequency, phase;

// index limits
int n0, n1, n2, n3, n4, n5;

// time values
double sim_time, d_t, time_end;
int n_recordings, recording_counter;
int n_avgs=500;
double n_oscil, weight;

// matrix solution variables
char trans = 'N';
int *ipiv;
int one=1, info;




fbin = fopen("iter_bin","rb");

fread(&time_end,sizeof(double),1,fbin);
fread(&d_t,sizeof(double),1,fbin);
fread(&n_recordings,sizeof(int),1,fbin);
n_recordings -= 1;
if(n_recordings < 1){
  n_recordings = 1;
}

sim_time = d_t;

len1 = 8;

totals = malloc(sizeof(int)*8);
fread(totals,sizeof(int),8,fbin);

n_emf_ac = totals[0];
n0 = n_emf_ac;
n_emf_dc = totals[1];
n1 = n0 + n_emf_dc;
n_res = totals[2];
n2 = n1 + n_res;
n_cap = totals[3];
n3 = n2 + n_cap;
n_ind = totals[4];
n4 = n3 + n_ind;
n_emf_spec = totals[5];
n5 = n4 + n_emf_spec;
n_elements = n5;

n_loops = totals[6];
ipiv = mkl_malloc(n_loops*sizeof(int),64);

len2 = n_elements;
repeats = malloc(sizeof(int)*len2);
single_AC_voltages = malloc(sizeof(double)*len2*3);
len3 = totals[7];
usage = malloc(sizeof(int)*len3);
len4 = n_elements + totals[6];
values = malloc(sizeof(double)*len4);
len5 = 2*totals[0];
if(len5 < 2){
	len5=2;
}
freq_and_phase = malloc(sizeof(double)*len5);
len6 = totals[3];
if(len6 < 1){
	len6=1;
}
capacitor_voltage = malloc(sizeof(double)*len6);


fread(repeats,sizeof(int),len2,fbin);
fread(usage,sizeof(int),len3,fbin);
fread(values,sizeof(double),len4,fbin);
fread(freq_and_phase,sizeof(double),len5,fbin);
fread(capacitor_voltage,sizeof(double),len6,fbin);
fclose(fbin);

if(totals[0]==1){
  n_oscil = (double)( (int)(freq_and_phase[0]*time_end)-n_avgs );
  weight = n_avgs/(2.0*freq_and_phase[0]*d_t);
}
for(i=0;i<totals[0];i++){
  freq_and_phase[2*i] = freq_and_phase[2*i]*twopi;
}

loop_current = malloc(sizeof(double)*n_loops);
loop_current_old = malloc(sizeof(double)*n_loops);
loop_voltage = malloc(sizeof(double)*n_loops);


imp_matrix = malloc(sizeof(double)*n_loops*n_loops);


// initialization
for(i=0;i<n_loops;i++){
  loop_current_old[i] = values[n5+i]; // from inputs, default is zero
  loop_voltage[i] = 0.0; // updated later, before simulation run
}
for(i=0;i<n_loops*n_loops;i++){
  imp_matrix[i]=0.0;
}

//structure for handling elements and INITIALIZING impedance matrix and voltage vector

pos = 0;
for(j=0;j<n0;j++){ // AC emf
  loops_used = repeats[j];
  frequency = freq_and_phase[2*j];
  phase = freq_and_phase[2*j+1];
  val = values[j]*cos(d_t*frequency + phase);
  for(i=0;i<loops_used;i++){
    loop1 = usage[pos+i];
    if(loop1 > 0){
      loop_voltage[loop1-1] += val;
    } else if (loop1 < 0){
      loop_voltage[-loop1-1] -= val;
    }
  }
  pos += loops_used;
}
for(j=n0;j<n1;j++){ // DC emf
  loops_used = repeats[j];
  val = values[j];
  for(i=0;i<loops_used;i++){
    loop1 = usage[pos+i];
    if(loop1 > 0){
      loop_voltage[loop1-1] += val;
    } else if (loop1 < 0){
      loop_voltage[-loop1-1] -= val;
    }
  }
  pos += loops_used;
}
res_pos = pos;
for(j=n1;j<n2;j++){ // resistors
  loops_used = repeats[j];
  val = values[j];
  for(i=0;i<loops_used;i++){
    loop1 = usage[pos+i];
    if(loop1 > 0){
      for(k=0;k<loops_used;k++){
        loop2 = usage[pos+k];
        if(loop2 > 0){
          imp_matrix[(loop1-1)*n_loops+loop2-1] += val;
        } else {
          imp_matrix[(loop1-1)*n_loops-loop2-1] -= val;
        }
      }
    } else {
      for(k=0;k<loops_used;k++){
        loop2 = usage[pos+k];
        if(loop2 < 0){
          imp_matrix[(-loop1-1)*n_loops-loop2-1] += val;
        } else {
          imp_matrix[(-loop1-1)*n_loops+loop2-1] -= val;
        }
      }
    }
  }
  pos += loops_used;
}
cap_pos = pos;
for(j=n2;j<n3;j++){ // capacitors
  loops_used = repeats[j];
  val = values[j];
  for(i=0;i<loops_used;i++){
    loop1 = usage[pos+i];
    if(loop1 > 0){
      loop_voltage[loop1] -= capacitor_voltage[j-n2];
      for(k=0;k<loops_used;k++){
        loop2 = usage[pos+k];
        if(loop2 > 0){
          imp_matrix[(loop1-1)*n_loops+loop2-1] += 0.5*d_t/val;
          loop_voltage[loop1-1] -= loop_current_old[loop2-1]*0.5*d_t/val;
        } else {
          imp_matrix[(loop1-1)*n_loops-loop2-1] -= 0.5*d_t/val;
          loop_voltage[loop1-1] += loop_current_old[-loop2-1]*0.5*d_t/val;
        }
      }
    } else {
      loop_voltage[-loop1-1] += capacitor_voltage[j-n2];
      for(k=0;k<loops_used;k++){
        loop2 = usage[pos+k];
        if(loop2 < 0){
          imp_matrix[(-loop1-1)*n_loops-loop2-1] += 0.5*d_t/val;
          loop_voltage[-loop1-1] -= loop_current_old[-loop2-1]*0.5*d_t/val;
        } else {
          imp_matrix[(-loop1-1)*n_loops+loop2-1] -= 0.5*d_t/val;
          loop_voltage[-loop1-1] += loop_current_old[loop2-1]*0.5*d_t/val;
        }
      }
    }
  }
  pos += loops_used;
}
for(j=n3;j<n4;j++){ // inductors
  loops_used = repeats[j];
  val = values[j];
  for(i=0;i<loops_used;i++){
    loop1 = usage[pos+i];
    if(loop1 > 0){
      for(k=0;k<loops_used;k++){
        loop2 = usage[pos+k];
        if(loop2 > 0){
          imp_matrix[(loop1-1)*n_loops+loop2-1] += val/d_t;
          loop_voltage[loop1-1] += loop_current_old[loop2-1]*val/d_t;
        } else {
          imp_matrix[loop1*n_loops-loop2] -= val/d_t;
          loop_voltage[loop1-1] -= loop_current_old[-loop2-1]*val/d_t;
        }
      }
    } else {
      for(k=0;k<loops_used;k++){
        loop2 = usage[pos+k];
        if(loop2 < 0){
          imp_matrix[(-loop1-1)*n_loops-loop2-1] += val/d_t;
          loop_voltage[-loop1-1] += loop_current_old[-loop2-1]*val/d_t;
        } else {
          imp_matrix[(-loop1-1)*n_loops+loop2-1] -= val/d_t;
          loop_voltage[-loop1-1] -= loop_current_old[loop2-1]*val/d_t;
        }
      }
    }
  }
  pos += loops_used;
}


for(j=n4;j<n5;j++){
  loops_used = repeats[j];
  values[j] = spec_func(j-n4,sim_time);
  val = values[j];
  for(i=0;i<loops_used;i++){
    loop1 = usage[pos+i];
    if(loop1 > 0){
      loop_voltage[loop1-1] += val;
    } else if (loop1 < 0){
      loop_voltage[-loop1-1] -= val;
    } else {
      i = loops_used+1;
    }
  }
  pos += loops_used;
}


// generate the pivot matrix

dgetrf_(&n_loops,&n_loops,imp_matrix,&n_loops,ipiv,&info);


fcurrent = fopen("iter_output_current.csv","w");
felement = fopen("iter_output_element_voltages.csv","w");
fprintf(fcurrent,"Time");
for(i=0;i<n_loops;i++){
  fprintf(fcurrent,",Loop %d",i+1);
}
fprintf(fcurrent,"\n");

fprintf(felement,"Time");
for(i=0;i<n0;i++){
  fprintf(felement,",AC %d",i+1);
}
for(i=n0;i<n1;i++){
  fprintf(felement,",DC %d",i+1-n0);
}
for(i=n1;i<n2;i++){
  fprintf(felement,",R %d",i+1-n1);
}
for(i=n2;i<n3;i++){
  fprintf(felement,",C %d",i+1-n2);
}
for(i=n3;i<n4;i++){
  fprintf(felement,",L %d",i+1-n3);
}
for(i=n4;i<n5;i++){
  fprintf(felement,",Special %d",i+1-n4);
}
fprintf(felement,"\n");


recording_counter = 0;

while(sim_time < time_end + d_t){

for(i=0;i<n_loops;i++){
  loop_current[i] = loop_voltage[i];
}

//solve matrix here, for Imp_Matrix * Current = Voltage

dgetrs_(&trans,&n_loops,&one,imp_matrix,&n_loops,ipiv,loop_current,&n_loops,&info);


// keep track of element voltage values for single driving EMF


if(totals[0] == 1){
if((sim_time+d_t >= twopi*n_oscil/freq_and_phase[0])&&(sim_time <= d_t+twopi*n_oscil/freq_and_phase[0])){
  phase = freq_and_phase[1];
  val = values[0];
  single_AC_voltages[0] = val;
  single_AC_voltages[1] = phase;
  single_AC_voltages[2] = 0.0;
  for(i=n0;i<n1;i++){
    single_AC_voltages[3*i] = 0.0;
    single_AC_voltages[3*i+1] = 0.0;
    single_AC_voltages[3*i+2] = 0.0;
  }
  pos = res_pos;
  for(i=n1;i<n2;i++){
    val = values[i];
    loops_used = repeats[i];
    resistor_voltage = 0.0;
    for(j=0;j<loops_used;j++){
      loop1 = usage[pos+j];
      if(loop1>0){
        resistor_voltage += val*loop_current[loop1-1];
      }else if(loop1<0){
        resistor_voltage -= val*loop_current[-loop1-1];
      }  
    }
    val = resistor_voltage;
    single_AC_voltages[3*i] = val;
    single_AC_voltages[3*i+1] = 0;
    single_AC_voltages[3*i+2] = 0;
    pos += loops_used;
  }
  for(i=n2;i<n3;i++){
    val = values[i];
    loops_used = repeats[i];
    capacitor_voltage_temp = capacitor_voltage[i-n2];
    for(j=0;j<loops_used;j++){
      loop1 = usage[pos+j];
      if(loop1>0){
        capacitor_voltage_temp += (loop_current[loop1-1] + loop_current_old[loop1-1])*d_t/(2.0*val);
      }else if(loop1<0){
        capacitor_voltage_temp -= (loop_current[-loop1-1] + loop_current_old[-loop1-1])*d_t/(2.0*val);
      }
    }
    val = capacitor_voltage_temp;
    single_AC_voltages[3*i] = val;
    single_AC_voltages[3*i+1] = 0;
    single_AC_voltages[3*i+2] = 0;
    pos += loops_used;
  }
  for(i=n3;i<n4;i++){
    val = values[i];
    loops_used = repeats[i];
    inductor_voltage = 0.0;
    for(j=0;j<loops_used;j++){
      loop1 = usage[pos+j];
      if(loop1>0){
        inductor_voltage += (loop_current[loop1-1] - loop_current_old[loop1-1])*val/d_t;
      }else if(loop1<0){
        inductor_voltage -= (loop_current[-loop1-1] - loop_current_old[-loop1-1])*val/d_t;
      }
    }
    val = inductor_voltage;
    single_AC_voltages[3*i] = val;
    single_AC_voltages[3*i+1] = 0;
    single_AC_voltages[3*i+2] = 0;
    pos += loops_used;
  }

  for(i=n4;j<n5;j++){
    val = values[i];
    single_AC_voltages[3*i] = val;
    single_AC_voltages[3*i+1] = 0;
    single_AC_voltages[3*i+2] = 0;
  }
}

if((sim_time >= twopi*n_oscil/freq_and_phase[0])&&(sim_time < twopi*(n_oscil+n_avgs)/freq_and_phase[0])){
  for(i=n0;i<n1;i++){
    val = values[i];
    if(single_AC_voltages[3*i] < val){
      single_AC_voltages[3*i] = val;
      single_AC_voltages[3*i+1] = fmod(sim_time*freq_and_phase[0] - n_oscil*twopi,twopi);
    }
    single_AC_voltages[3*i+2] += val/weight;
    pos += loops_used;
  }
  pos = res_pos;
  for(i=n1;i<n2;i++){
    val = values[i];
    loops_used = repeats[i];
    resistor_voltage = 0.0;
    for(j=0;j<loops_used;j++){
      loop1 = usage[pos+j];
      if(loop1>0){
        resistor_voltage += val*loop_current[loop1-1];
      }else if(loop1<0){
        resistor_voltage -= val*loop_current[-loop1-1];
      }  
    }
    
    val = resistor_voltage;
    if(single_AC_voltages[3*i] < val){
      single_AC_voltages[3*i] = val;
      single_AC_voltages[3*i+1] = fmod(sim_time*freq_and_phase[0] - n_oscil*twopi,twopi);
    }
    single_AC_voltages[3*i+2] += val/weight;
    pos += loops_used;
  }
  for(i=n2;i<n3;i++){
    val = values[i];
    loops_used = repeats[i];
    capacitor_voltage_temp = capacitor_voltage[i-n2];
    for(j=0;j<loops_used;j++){
      loop1 = usage[pos+j];
      if(loop1>0){
        capacitor_voltage_temp += (loop_current[loop1-1] + loop_current_old[loop1-1])*d_t/(2.0*val);
      }else if(loop1<0){
        capacitor_voltage_temp -= (loop_current[-loop1-1] + loop_current_old[-loop1-1])*d_t/(2.0*val);
      }
    }
    val = capacitor_voltage_temp;
    if(single_AC_voltages[3*i] < val){
      single_AC_voltages[3*i] = val;
      single_AC_voltages[3*i+1] = fmod(sim_time*freq_and_phase[0] - n_oscil*twopi,twopi);
    }
    single_AC_voltages[3*i+2] += val/weight;
    pos += loops_used;
  }
  for(i=n3;i<n4;i++){
    val = values[i];
    loops_used = repeats[i];
    inductor_voltage = 0.0;
    for(j=0;j<loops_used;j++){
      loop1 = usage[pos+j];
      if(loop1>0){
        inductor_voltage += (loop_current[loop1-1] - loop_current_old[loop1-1])*val/d_t;
      }else if(loop1<0){
        inductor_voltage -= (loop_current[-loop1-1] - loop_current_old[-loop1-1])*val/d_t;
      }
    }
    val = inductor_voltage;
    if(single_AC_voltages[3*i] < val){
      single_AC_voltages[3*i] = val;
      single_AC_voltages[3*i+1] = fmod(sim_time*freq_and_phase[0] - n_oscil*twopi,twopi);
    }
    single_AC_voltages[3*i+2] += val/weight;
    pos += loops_used;
  }

  for(i=n4;j<n5;j++){
    val = values[i];
    if(single_AC_voltages[3*i] < val){
      single_AC_voltages[3*i] = val;
      single_AC_voltages[3*i+1] = fmod(sim_time*freq_and_phase[0] - n_oscil*twopi,twopi);
    }
    single_AC_voltages[3*i+2] += val/weight;
  }

}
} // end if totals[0] == 1

//perform recording of voltages across elements


if(sim_time > recording_counter*time_end/n_recordings){
  fprintf(fcurrent, "%lf", sim_time);
  for(i=0;i<n_loops;i++){
    fprintf(fcurrent, ",%lf", loop_current[i]);
  }
  fprintf(fcurrent, "\n");
  fprintf(felement, "%lf", sim_time);
  for(i=0;i<n0;i++){
    frequency = freq_and_phase[2*i];
    phase = freq_and_phase[2*i+1];
    val = values[i]*cos(sim_time*frequency+phase);
    fprintf(felement, ",%lf", val);
  }
  for(i=n0;i<n1;i++){
    fprintf(felement, ",%lf", values[i]);
  }
  pos = res_pos;
  for(i=n1;i<n2;i++){
    val = values[i];
    loops_used = repeats[i];
    resistor_voltage = 0.0;
    for(j=0;j<loops_used;j++){
      loop1 = usage[pos+j];
      if(loop1>0){
        resistor_voltage += val*loop_current[loop1-1];
      }else if(loop1<0){
        resistor_voltage -= val*loop_current[-loop1-1];
      }  
    }
    fprintf(felement, ",%lf", resistor_voltage);
    pos += loops_used;
  }
  for(i=n2;i<n3;i++){
    val = values[i];
    loops_used = repeats[i];
    for(j=0;j<loops_used;j++){
      loop1 = usage[pos+j];
      if(loop1>0){
        capacitor_voltage[i-n2] += (loop_current[loop1-1] + loop_current_old[loop1-1])*d_t/(2.0*val);
      }else if(loop1<0){
        capacitor_voltage[i-n2] -= (loop_current[-loop1-1] + loop_current_old[-loop1-1])*d_t/(2.0*val);
      }
    }
    fprintf(felement, ",%lf", capacitor_voltage[i-n2]);
    pos += loops_used;
  }
  for(i=n3;i<n4;i++){
    val = values[i];
    loops_used = repeats[i];
    inductor_voltage = 0.0;
    for(j=0;j<loops_used;j++){
      loop1 = usage[pos+j];
      if(loop1>0){
        inductor_voltage += (loop_current[loop1-1] - loop_current_old[loop1-1])*val/d_t;
      }else if(loop1<0){
        inductor_voltage -= (loop_current[-loop1-1] - loop_current_old[-loop1-1])*val/d_t;
      }
    }
    fprintf(felement, ",%lf", inductor_voltage);
    pos += loops_used;
  }

  for(i=n4;j<n5;j++){
    fprintf(felement, ",%lf", values[i]);
  }

  recording_counter++;
  fprintf(felement, "\n");
}else{
  pos = cap_pos;
  for(i=n2;i<n3;i++){
    val = values[i];
    loops_used = repeats[i];
    for(j=0;j<loops_used;j++){
      loop1 = usage[pos+j];
      if(loop1>0){
        capacitor_voltage[i-n2] += (loop_current[loop1-1] + loop_current_old[loop1-1])*d_t/(2.0*val);
      }else if(loop1<0){
        capacitor_voltage[i-n2] -= (loop_current[-loop1-1] + loop_current_old[-loop1-1])*d_t/(2.0*val);
      }
    }
    pos += loops_used;
  }
}

//structure for handling elements and UPDATING (recalculating) voltage vector
sim_time += d_t;

for(i=0;i<n_loops;i++){
  loop_voltage[i] = 0.0;
  loop_current_old[i] = loop_current[i];
}

pos = 0;
for(j=0;j<n0;j++){ // AC emf
  loops_used = repeats[j];
  frequency = freq_and_phase[2*j];
  phase = freq_and_phase[2*j+1];
  val = values[j]*cos(phase + sim_time*frequency);
  for(i=0;i<loops_used;i++){
    loop1 = usage[pos+i];
    if(loop1 > 0){
      loop_voltage[loop1-1] += val;
    } else if (loop1 < 0){
      loop_voltage[-loop1-1] -= val;
    }
  }
  pos += loops_used;
}

for(j=n0;j<n1;j++){ // DC emf
  loops_used = repeats[j];
  val = values[j];
  for(i=0;i<loops_used;i++){
    loop1 = usage[pos+i];
    if(loop1 > 0){
      loop_voltage[loop1-1] += val;
    } else if (loop1 < 0){
      loop_voltage[-loop1-1] -= val;
    }
  }
  pos += loops_used;
}

// resistors not used in voltage, only current
pos = cap_pos;

for(j=n2;j<n3;j++){ // capacitors
  loops_used = repeats[j];
  val = values[j];
  for(i=0;i<loops_used;i++){
    loop1 = usage[pos+i];
    if(loop1 > 0){
      loop_voltage[loop1-1] -= capacitor_voltage[j-n2];
      for(k=0;k<loops_used;k++){
        loop2 = usage[pos+k];
        if(loop2 > 0){
          loop_voltage[loop1-1] -= loop_current_old[loop2-1]*0.5*d_t/val;
        } else {
          loop_voltage[loop1-1] += loop_current_old[-loop2-1]*0.5*d_t/val;
        }
      }
    } else {
      loop_voltage[-loop1-1] += capacitor_voltage[j-n2];
      for(k=0;k<loops_used;k++){
        loop2 = usage[pos+k];
        if(loop2 < 0){
          loop_voltage[-loop1-1] -= loop_current_old[-loop2-1]*0.5*d_t/val;
        } else {
          loop_voltage[-loop1-1] += loop_current_old[loop2-1]*0.5*d_t/val;
        }
      }
    }
  }
  pos += loops_used;
}
for(j=n3;j<n4;j++){ // inductors
  loops_used = repeats[j];
  val = values[j];
  for(i=0;i<loops_used;i++){
    loop1 = usage[pos+i];
    if(loop1 > 0){
      for(k=0;k<loops_used;k++){
        loop2 = usage[pos+k];
        if(loop2 > 0){
          loop_voltage[loop1-1] += loop_current_old[loop2-1]*val/d_t;
        } else {
          loop_voltage[loop1-1] -= loop_current_old[-loop2-1]*val/d_t;
        }
      }
    } else {
      for(k=0;k<loops_used;k++){
        loop2 = usage[pos+k];
        if(loop2 < 0){
          loop_voltage[-loop1-1] += loop_current_old[-loop2-1]*val/d_t;
        } else {
          loop_voltage[-loop1-1] -= loop_current_old[loop2-1]*val/d_t;
        }
      }
    }
  }
  pos += loops_used;
}
 // Here begin the special EMF functions
for(j=n4;j<n5;j++){
  loops_used = repeats[j];
  val = spec_func(j-n4,sim_time);
  for(i=0;i<loops_used;i++){
    loop1 = usage[pos+i];
    if(loop1 > 0){
      loop_voltage[loop1-1] += val;
    } else if (loop1 < 0){
      loop_voltage[-loop1-1] -= val;
    } else {
      i = loops_used+1;
    }
  }
  pos += loops_used;
}


sim_time += d_t;

} // end while

fclose(fcurrent);
fclose(felement);

fsingleAC = fopen("single_AC_voltages.csv","w");
if(totals[0] == 1){
  fprintf(fsingleAC,"Frequency:,%lf\n", freq_and_phase[0]);
  fprintf(fsingleAC,"Element#,amplitude,phase,mean\n");
  for(i=0;i<n_elements;i++){
      single_AC_voltages[3*i] -= single_AC_voltages[3*i+2];
      fprintf(fsingleAC,"%d,%lf,",i+1,single_AC_voltages[3*i]);
      fprintf(fsingleAC,"%lf,%lf\n",single_AC_voltages[3*i+1],single_AC_voltages[3*i+2]);

  }
}
fclose(fsingleAC);

return 0;
} // end main()






