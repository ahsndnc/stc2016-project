#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(){

int *repeats; // matrix which identifies how many loops each element is associated with
int *usage; // usage matrix (which loops are associated with the given elements)
int *totals; // totals vector (totals of loops, special emf, ac emf, dc emf, res, cap, ind)
double *values;
double *freq_and_phase;
double *capacitor_voltage;
FILE *fp;
FILE *fbin;
int n_loops, n_elements, n_emf_spec, n_emf_ac, n_emf_dc, n_res, n_cap, n_ind, n_connect;
int i, j, k, ii, jj, kk, m, n, mm, nn;
int n_recordings;
double val_dub, time_tot, time_step;
int val_int, endline, space;
char letter, stringval[100];


//iter_counter(); // function defined in iter_counter.c
// finds n_loops, n_(elements), and n_connects
// rewrites these values if necessary

fprintf(stdout, "iter_reader running\n");
fflush(stdout);

fp = fopen("./iter_input.txt","r");

totals = malloc(sizeof(int)*8);

n_elements = 0;

fprintf(stdout,"Reading element counts\n");
fflush(stdout);


memset(stringval, '\0', sizeof(stringval));
for(i=0;i<3;i++){
    while((letter = fgetc(fp)) != '\n'){
    }
}

k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "total_simulation_time= %lf", &time_tot); //parse for first entry, n_loops

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "time_step= %lf", &time_step);

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_recordings= %d", &n_recordings);

for(i=0;i<2;i++){
    while((letter = fgetc(fp)) != '\n'){
    }
}

k=0;
memset(stringval, '\0', strlen(stringval));
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_loops= %d", &n_loops);
totals[6] = n_loops;

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_emfs_ac= %d", &n_emf_ac);
totals[0] = n_emf_ac;
n_elements += n_emf_ac;
freq_and_phase = malloc(sizeof(double)*n_emf_ac*2);

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_emfs_dc= %d", &n_emf_dc);
totals[1] = n_emf_dc;
n_elements += n_emf_dc;

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_resistors= %d", &n_res);
totals[2] = n_res;
n_elements += n_res;

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_capacitors= %d", &n_cap);
totals[3] = n_cap;
n_elements += n_cap;
capacitor_voltage = malloc(sizeof(double)*totals[3]);

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_inductors= %d", &n_ind);
totals[4] = n_ind;
n_elements += n_ind;

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_emfs_special= %d", &n_emf_spec);
totals[5] = n_emf_spec;
n_elements += n_emf_spec;

memset(stringval, '\0', strlen(stringval));
k=0;
while((letter = fgetc(fp)) != '\n'){
    stringval[k] = letter;
    k++;
}
sscanf(stringval, "n_connections= %d", &n_connect);
totals[7] = n_connect;


repeats = malloc(sizeof(int)*n_elements);
usage = malloc(sizeof(int)*totals[7]);
values = malloc(sizeof(double)*(n_elements+totals[6]));

//stringval = empty
ii = 0;
jj = 0;
nn = 0;
space = 0;
for(i=0;i<7;i++){
    kk = 0;
    fprintf(stdout,"Reading element values, %d / 7...",i+1);
    fflush(stdout);
    memset(stringval, '\0', sizeof(stringval));
    for(mm=0;mm<2;mm++){
        while((letter = fgetc(fp)) != '\n'){
        }
    }
    for(j=0;j<totals[i];j++){
        endline = 0;
        if(nn < n_elements){
            repeats[nn] = 0;
        }
        k = 0;
        while(space == 0){
            letter = fgetc(fp);//section that reads character (letter)
            if(letter == ' '){
                space = 1;
                sscanf(stringval, "%lf", &val_dub);
                values[ii] = val_dub;
                ii++;
                k=0;
                memset(stringval, '\0', strlen(stringval));
            }else if(letter != '\n'){
                stringval[k] = letter;
                k++;
            }else{
                space = 1;
                endline = 1;
                sscanf(stringval, "%lf", &val_dub);
                values[ii] = val_dub;
                ii++;
                k=0;
                memset(stringval, '\0', strlen(stringval));                
            }
        }
        space = 0;
        if(i == 0){
            for(m=0;m<2;m++){
                while(space == 0){
                    letter = fgetc(fp);//section that reads character (letter)
                    if(letter == ' '){
                        space = 1;
                        sscanf(stringval, "%lf", &val_dub);
                        freq_and_phase[kk] = val_dub;
                        kk++;
                        k=0;
                        memset(stringval, '\0', strlen(stringval));
                    }else if(letter != '\n'){
                        stringval[k] = letter;
                        k++;
                    }else{
                        space = 1;
                        endline = 1;
                        sscanf(stringval, "%lf", &val_dub);
                        freq_and_phase[kk] = val_dub;
                        kk++;
                        k=0;
                        memset(stringval, '\0', strlen(stringval));
                    }
                }
                space = 0;
            }
        }else if(i == 3){
            while(space == 0){
                letter = fgetc(fp);//section that reads character (letter)
                if(letter == ' '){
                    space = 1;
                    sscanf(stringval, "%lf", &val_dub);
                    capacitor_voltage[kk] = val_dub;
                    kk++;
                    k=0;
                    memset(stringval, '\0', strlen(stringval));
                }else if(letter != '\n'){
                    stringval[k] = letter;
                    k++;
                }else{
                    space = 1;
                    endline = 1;
                    sscanf(stringval, "%lf", &val_dub);
                    capacitor_voltage[kk] = val_dub;
                    kk++;
                    k=0;
                    memset(stringval, '\0', strlen(stringval));
                }
            }
            space = 0;
        }

        space = 0;
        while(endline == 0){
            k = 0;
            while(space == 0){
                letter = fgetc(fp);//section that reads character(letter)
                if(letter == ' '){
                    space = 1;
                    sscanf(stringval, "%d", &val_int);
                    usage[jj] = val_int;
                    if(nn<n_elements){
                        repeats[nn] += 1;
                    }
                    jj++;
                    k=0;
                    memset(stringval, '\0', strlen(stringval));
                } else if(letter != '\n'){
                    stringval[k] = letter;
                    k++;
                } else {
                    space = 1;
                    endline = 1;
                    sscanf(stringval, "%d", &val_int);
                    usage[jj] = val_int;
                    if(nn<n_elements){
                        repeats[nn] += 1;
                    }
                    jj++;
                    k=0;
                    memset(stringval, '\0', strlen(stringval));
                }

            }
            space = 0;
        }
        endline = 0;
        if(nn<n_elements){
            nn++;
        }
    }
fprintf(stdout," done!\n");
fflush(stdout);
}

fclose(fp);

fprintf(stdout,"Writing binaries\n");
fflush(stdout);



fbin = fopen("iter_bin","wb");
fwrite(&time_tot,sizeof(double),1,fbin);
fwrite(&time_step,sizeof(double),1,fbin);
fwrite(&n_recordings,sizeof(int),1,fbin);
fwrite(totals,sizeof(int),8,fbin);
fwrite(repeats,sizeof(int),n_elements,fbin);
fwrite(usage,sizeof(int),totals[7],fbin);
fwrite(values,sizeof(double),n_elements+totals[6],fbin);
fwrite(freq_and_phase,sizeof(double),2*totals[0],fbin);
fwrite(capacitor_voltage,sizeof(double),totals[3],fbin);
fclose(fbin);

} //end of main



