#include <stdlib.h>
#include <math.h>
#include "special_emf.h"

double spec_func(int elementno, double t){

double f_t = 0.0;

switch(elementno){
    case 1:
        f_t = t;
    case 2:
        f_t = 0.0;
    case 3:
        f_t = 0.0;

    // Include more cases here to extend functionality

}

return f_t;
}

