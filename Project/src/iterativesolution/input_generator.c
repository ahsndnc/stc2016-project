#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main(){


FILE *fbin;
int width, height, v_plus_x, v_plus_y, v_minus_x, v_minus_y;
double res;
int n_loops, n_connect, n_res, temp_int, i, j;
int *connections;

res = 100.0; // resistance

v_plus_x = 5;
v_plus_y = 5;
v_minus_x = 10;
v_minus_y = 10;

if(v_plus_x < v_minus_x){
  temp_int = v_plus_x;
  v_plus_x = v_minus_x;
  v_minus_x = temp_int;
}
if(v_plus_y < v_minus_y){
  temp_int = v_plus_y;
  v_plus_y = v_minus_y;
  v_minus_y = temp_int;
}


width = 14;
height = 14;
n_loops = (width-1)*(height-1);
// start by only counting loops with only resistors. Others will be included later

n_res = n_loops*2 + (width + height - 2);
// each loop has 2, one below and one to the right,
// and then the top row and left row have an extra above or right

n_connect = n_loops*4;
// each loop with only resistors has 4 resistors. We add more later

// now we add the loop with the voltage element
n_loops += 1;
// it has one connection for the voltage element, and one for each resistor on the path
n_connect += 1 + abs(v_plus_x - v_minus_x) + abs(v_plus_y - v_minus_y);

fbin = fopen("premade_iter_input.txt","w");

fprintf(fbin,"See README file for clarification on input layout\n");
fprintf(fbin,"\n");
fprintf(fbin,"TIME VALUES (s)\n");
fprintf(fbin,"total_simulation_time= 10.0\n");
fprintf(fbin,"time_step= 0.1\n");
fprintf(fbin,"n_recordings= 10\n");
fprintf(fbin,"\n");
fprintf(fbin,"LOOP/ELEMENT TOTALS\n");
fprintf(fbin,"n_loops= %d\n",n_loops);
fprintf(fbin,"n_emfs_ac= 0\n");
fprintf(fbin,"n_emfs_dc= 1\n");
fprintf(fbin,"n_resistors= %d\n",n_res);
fprintf(fbin,"n_capacitors= 0\n");
fprintf(fbin,"n_inductors= 0\n");
fprintf(fbin,"n_emfs_special= 0\n");
fprintf(fbin,"n_connections= %d\n",n_connect);
fprintf(fbin,"\n");
fprintf(fbin,"AC EMFS (Voltage (V), Frequency (Hz), Initial Phase (radians), loop#1, ..., loop #n)\n");
fprintf(fbin,"\n");
fprintf(fbin,"DC EMFS (voltage (V), loop #1, ..., loop #n)\n");
fprintf(fbin,"100.0 %d\n",n_loops);
fprintf(fbin,"\n");
fprintf(fbin,"RESISTORS (resistance (Ohms), loop #1, ..., loop #n)\n");

for(i=0;i<width-1;i++){
  fprintf(fbin,"%lf %d\n",res,1+i);
  fprintf(fbin,"%lf %d\n",res,1+i+(width-1)*(height-2));
}
for(i=0;i<height-1;i++){
  fprintf(fbin,"%lf %d\n",res,1+(width-1)*i);
  fprintf(fbin,"%lf %d\n",res,1+(width-1)*(i+1)-1);
}
// (W-2)*(H-1) vertical interior and (W-1)*(H-2) horizontal interior
if((v_plus_x >= v_minus_x) && (v_plus_y >= v_minus_y)){
  for(i=0;i<width-2;i++){  // vertical
    for(j=0;j<height-1;j++){
      if((i+1 == v_minus_x) &&(j>=v_minus_y) &&(j < v_plus_y) ){
        fprintf(fbin,"%lf %d -%d -%d\n",res,1+i+j*(width-1),1+i+1+j*(width-1),n_loops);
      }else{
        fprintf(fbin,"%lf %d -%d\n",res,1+i+j*(width-1),1+i+1+j*(width-1));
      }
    }
  }
  for(j=0;j<height-2;j++){ // horizontal
    for(i=0;i<width-1;i++){
      if((j+2 == v_plus_y) &&(i>=v_minus_x) &&(i < v_plus_x) ){
        fprintf(fbin,"%lf %d -%d %d\n",1+i+j*(height-1),1+i+(j+1)*(height-1),n_loops);
      }else{
        fprintf(fbin,"%lf %d -%d\n",1+i+j*(height-1),1+i+(j+1)*(height-1));
      }
    }
  }
}


fprintf(fbin,"\n");
fprintf(fbin,"CAPACITORS (capacitance (F), initial voltage(V), loop #1, ..., loop #n)\n");
fprintf(fbin,"\n");
fprintf(fbin,"INDUCTORS (inductance (H), loop #1, ..., loop #n)\n");
fprintf(fbin,"\n");
fprintf(fbin,"SPECIAL EMFS (Placeholder voltage = 0.0, loop #1, ..., loop #n)\n");
fprintf(fbin,"\n");
fprintf(fbin,"INITIAL CURRENTS (amperes, default is 0.0, NOTE: leave two endlines after finish)\n");
for(i=0;i<n_loops;i++){
  fprintf(fbin,"0.0\n");
}
fprintf(fbin,"\n");
fprintf(fbin,"\n");

}//end main


